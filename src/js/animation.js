'use strict';
~ function() {
    var $ = TweenMax,
    	noOfPlash = 6,
    	sineOut = Ease.sineOut,
        clipPath = document.querySelector("#arcPath"),
        RAD  = Math.PI / 180,
        PI_2 = Math.PI / 2,
        arc = {  
          start: 0,
          end: 360,
          cx: 150,
          cy: 125,
          r: 200 
        },
        ad = document.getElementById('mainContent');

    window.init = function() {

        //creating colour Plash elements
        for (var i = 1; i < noOfPlash+1; i++) {
        	var plashImg = document.createElement("img");
            var imgName = "img/colourPlash" + i + ".png";
        		plashImg.src = imgName;
        		plashImg.className = "plash";
        		plashContainer.appendChild(plashImg);
        }

        var tl = new TimelineMax();
        tl.set(ad, { perspective: 1000, force3D: true })

        //colour Plash Animation
        .staggerTo(".plash", .2, { opacity: 1, scale:1, ease: sineOut }, 0.1)
        .to(["#copyOne","#plashContainer"], 0.7, { x:-300, ease:sineOut }, "+=2")

        .to("#frameTwo", 0.7, { x:-300, ease:sineOut }, "+=1")

        //playing reveal Animation
        .to(arc, 1.5, {end: 0, ease: Linear.easeNone, onUpdate: updatePath}, "+=1")

        .to("#frameThree", 0.7, { x:-300, ease:sineOut }, "+=1")

        }

        // updating circle svg
        function updatePath() {
          clipPath.setAttribute("d", getPath(arc.cx, arc.cy, arc.r, arc.start, arc.end)); 
        }

        function getPath(cx, cy, r, a1, a2) {
           
          var delta = a2 - a1;
          
          if (delta === 360) {
                
            return "M " + (cx - r) + " " + cy + " a " + r + " " + r + " 0 1 0 " + r * 2 + " 0 a " + r + " " + r + " 0 1 0 " + -r * 2 + " 0z"; 
          }
          
          var largeArc = delta > 180 ? 1 : 0;
            
          a1 = a1 * RAD - PI_2;
          a2 = a2 * RAD - PI_2;

          var x1 = cx + r * Math.cos(a2);   
          var y1 = cy + r * Math.sin(a2);

          var x2 = cx + r * Math.cos(a1); 
          var y2 = cy + r * Math.sin(a1);
            
          return "M " + x1 + " " + y1 + " A " + r + " " + r + " 0 " + largeArc + " 0 " + x2 + " " + y2 + " L " + cx + " " + cy + "z";
        }



}();
